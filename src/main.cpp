/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
//#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>

#include <QStandardPaths>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtQml>
#include <QQmlComponent>
#include <QIcon>

#include <QThread>

#include "backend/ContactsDatabase.h"
#include "backend/MessageDatabase.h"
#include "backend/QiTalkAPI.h"

//#define DEBUG

int main(int argc, char *argv[])
{
    // Get Directory for interfacing
    const QString portname = argc > 1? argv[1] : "/dev/ttyS0";
    qDebug() << "portname:" << portname;

    // Create core stuff
    /* Now using Matchbox Keyboard */
    //qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    const QString default_icon = "/usr/share/icons/QiTalk_icon.png";
    app.setWindowIcon(QIcon(default_icon));

    qmlRegisterType<ContactsDatabase>("fishandmicrochips.qitalk.database", 1, 0, "ContactsDatabase");
    qmlRegisterType<MessageDatabase> ("fishandmicrochips.qitalk.database", 1, 0, "MessageDatabase");
    qmlRegisterType<QiTalkAPI>       ("fishandmicrochips.qitalk.api",      1, 0, "QiTalkAPI");


    // Set up ~/.local/share/QiTalk
    const QDir write_dir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    if (!write_dir.mkpath("."))
        qFatal("Failed to create a writable directory at %s", qPrintable(write_dir.absolutePath()));

    /* Connect to SQL Database */
    {
        qDebug() << "Connecting to database...";
        QSqlDatabase database = QSqlDatabase::database();
        if (!database.isValid()) {
            database = QSqlDatabase::addDatabase("QSQLITE");
            if (!database.isValid())
                qFatal("Error creating database: %s", qPrintable(database.lastError().text()));
        }

        const QString filename = write_dir.absolutePath() + "/chat-database.sqlite3";
        database.setDatabaseName(filename);

        if (!database.open()) {
            QFile::remove(filename);
            qFatal("Cannot open database: %s", qPrintable(database.lastError().text()));
        }
        qDebug() << "Done.";
    }

    QQmlApplicationEngine engine;

    /* Set up icons path */
    {
        const QString icons_folder = "icons";
        if (!write_dir.mkpath(icons_folder))
            qFatal("Failed to create the icons directory at %s", qPrintable(write_dir.absolutePath()));

        const QString icons_path = QString("file://%1/%2/").arg(write_dir.absolutePath()).arg(icons_folder);
        // Put the path for finding icons inside the QML engine
        engine.rootContext()->setContextProperty("iconsPath", icons_path);
    }

    QiTalkAPI api(nullptr, portname);
    ContactsDatabase contacts_database;
    MessageDatabase message_database;

    QObject::connect(&api, &QiTalkAPI::smsSent,      &message_database, &MessageDatabase::sendMessage);
    QObject::connect(&api, &QiTalkAPI::smsReceived,  &message_database, &MessageDatabase::receiveMessage);
    QObject::connect(&api, &QiTalkAPI::callStarted,  &message_database, &MessageDatabase::callStarted);
    QObject::connect(&api, &QiTalkAPI::callEnded,    &message_database, &MessageDatabase::callEnded);
    QObject::connect(&api, &QiTalkAPI::incomingCall, &message_database, &MessageDatabase::incomingCall);
    QObject::connect(&api, &QiTalkAPI::outgoingCall, &message_database, &MessageDatabase::outgoingCall);
#ifdef DEBUG
    QObject::connect(&api, &QiTalkAPI::written,      &message_database, &MessageDatabase::command_written);
    QObject::connect(&api, &QiTalkAPI::read,         &message_database, &MessageDatabase::command_received);
#endif

    QObject::connect(&message_database, &MessageDatabase::newContact, &contacts_database, &ContactsDatabase::newContact);

    engine.rootContext()->setContextProperty("contactsDatabase", &contacts_database);
    engine.rootContext()->setContextProperty("messageDatabase", &message_database);
    engine.rootContext()->setContextProperty("api", &api);


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
