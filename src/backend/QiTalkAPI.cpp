/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#include "QiTalkAPI.h"
#include <QDebug>
#include <QRegularExpressionMatch>
#include <QTextCodec>

QiTalkAPI::QiTalkAPI(QObject *parent, const QString port_name) : QObject(parent)
{
    /* BUILD PHASE */
    call_poller.setInterval(6000);
    short_meta_poller.setInterval(6000);
    long_meta_poller.setInterval(60000);
    power_poller.setInterval(1000);

    /* CONNECT PHASE */
    connect(&serial, &QSerialPort::readyRead, this, &QiTalkAPI::handleReadyRead);
    connect(&call_poller, &QTimer::timeout, this, &QiTalkAPI::pollCallStatus);
    connect(&long_meta_poller, &QTimer::timeout, this, &QiTalkAPI::pollLongMeta);
    connect(&short_meta_poller, &QTimer::timeout, this, &QiTalkAPI::pollShortMeta);
    connect(&power_poller, &QTimer::timeout, this, &QiTalkAPI::pollPower);

    /* RUN PHASE */
    serial.setPortName(port_name);
    open();
}

QiTalkAPI::~QiTalkAPI()
{
    setPower(false);
}

/* SMS FUNCTIONS */

QString QiTalkAPI::sendSMS(const QString num, const QString body)
{
    // Step 0: Specify that we're going to send a text to <num>
    sending_sms = true;
    const QString sms_prompt = "> "; // When we send a request to send a text, this is what we get in return.
    if (request(cmd.send_sms.arg(num), true).last() != sms_prompt) {
        qFatal("Error sending text. Please restart by pressing the physical switch on your PiTalk");
    }
    //const QString body = utf16ToHexString(sms_body);
    // Step 1: Send the body of the text
    const QVector<QString> sms_response = request(body + sms_term + "\r\n", false, 400);
    const QString current_date_time = QDateTime::currentDateTime().toString(Qt::ISODate);
    if (sms_response.last() != "OK") {
        qWarning() << "Failed to send text to" << num << ":" << body;
        emit smsSent(num, sms_response.last(), current_date_time);
    } else {
        emit smsSent(num, body, current_date_time);
    }
    sending_sms = false;
    return sms_response.last();
}

bool QiTalkAPI::isSMS(const QVector<QString> read_data) { return regex.new_sms.match(read_data.first()).hasMatch(); }

void QiTalkAPI::handleIncomingSMS(const QVector<QString> read_data)
{
    /* Get the SMS data. More specifically, get the phone number, message, and time of message stored in a vector */
    const QString sms_index = regex.new_sms.match(read_data.first()).captured(2);
    const QVector<QString> sms_data = request(cmd.receive_sms.arg(sms_index));

    if (sms_data.last() != "OK") {
        return;
    }

    /* Get the metadata*/
    qDebug() << "Grabbing SMS metadata...";
    const QVector<QString> sms_metadata = sms_data[1].split("\"").toVector();
    if (sms_metadata[3] == "CONSOLE") {
        return;
    }

    setTextNum(toInternational(sms_metadata[3]));
    /*
     * Unfortunately, the truncation of the year format means we have to get the first two digits of the year.
     * This means fetching it from the current time. While this is technically filling in the gaps with an assumption,
     * If your text takes a century to send, I'd like to congratulate humanity on the achievement of interstellar travel.
     */
    QString current_century = QDateTime::currentDateTime().toString("yyyy");
    current_century.remove(current_century.size()-2, 2);

    setTextTime(QDateTime::fromString(current_century + sms_metadata[5].split("+").at(0), "yyyy/MM/dd,hh:mm:ss").toString(Qt::ISODate));

    /* Get the full text body */
    QString text_body = sms_data[2];
    for (int i = 3, sz = sms_data.size() - 2; i < sz; ++i) {
        text_body += "\n" + sms_data[i];
    }

    if (isWrongEncoding(text_body)) {
        text_body = hexStringToUtf16(text_body);
    }

    setTextBody(text_body);

    /* Delete the SMS from the modem's memory*/
    if (request(cmd.delete_sms.arg(sms_index)).last() != "OK") {
        qWarning() << "Error deleting message from modem's internal storage";
    }

    emit smsReceived(text_num, text_body, text_time);
}

QString QiTalkAPI::getTextNum() const { return text_num; }
void QiTalkAPI::setTextNum(const QString new_text_num)
{
    if (new_text_num == text_num)
        return;
    text_num = new_text_num;
    emit textNumChanged();
}

QString QiTalkAPI::getTextBody() const { return text_body; }
void QiTalkAPI::setTextBody(const QString new_text_body)
{
    if (new_text_body == text_body)
        return;
    text_body = new_text_body;
    emit textBodyChanged();
}

QString QiTalkAPI::getTextTime() const { return text_time; }
void QiTalkAPI::setTextTime(const QString new_text_time)
{
    if (new_text_time == text_time)
        return;
    text_time = new_text_time;
    emit textTimeChanged();
}

/* CALL FUNCTIONS */

bool QiTalkAPI::isCall(const QVector<QString> read_data)
{
    return regex.new_call.match(read_data.first()).hasMatch();
}

void QiTalkAPI::handleIncomingCall(const QVector<QString> read_data)
{
    qDebug() << "QiTalkAPI::handleIncomingCall(): Incoming call!";
    if (read_data.size() < 3) {
        qDebug() << "QiTalkAPI::handleIncomingCall(): Error with the incoming call command: Format error";
        return;
    }
    if (call_status != ring_incoming_call) {
        call_num = toInternational(regex.call_details.match(read_data[2]).captured(1));
        call_status = ring_incoming_call;
        emit callNumChanged();
        emit incomingCall(call_num);
        call_poller.start();
    }
}

QString QiTalkAPI::getCallNum() const
{
    return call_num;
}

void QiTalkAPI::pollCallStatus()
{
    const QVector<QString> poll_call_data = request(cmd.call_data);
    if (poll_call_data.last() != "OK") {
        qWarning() << "QiTalkAPI::pollCallStatus(): Error getting CPAS data (Call status)";
        return;
    }

    const QString cpas_status = regex.call_poll.match(poll_call_data[1]).captured(1);

    if (cpas_status == "0") {
        emit callEnded(call_num);
        call_status = not_in_call;
        call_poller.stop();
    } else if ((cpas_status == "4") && (call_status != in_call)) {
        qDebug() << "Starting call...";
        call_status = in_call;
        emit inCallChanged();
        emit callStarted(call_num);
    }
}

QString QiTalkAPI::hangup()
{
    call_status = not_in_call;
    emit inCallChanged();
    return request(cmd.hangup).last();
}

QString QiTalkAPI::connectCall()
{
    return request(cmd.connect).last();
}

QString QiTalkAPI::dialInCall(const QString num)
{
    return call_status == in_call? request(cmd.dial_in_call.arg(num)).last() : "NOT IN CALL";
}

QString QiTalkAPI::dial(const QString num)
{
    call_num = toInternational(num);
    const QVector<QString> dial_status = request(cmd.dial.arg(call_num));

    if (!validNum(call_num)) {
        return "ERROR";
    }
    if (call_status != ring_outgoing_call) {
        call_status = ring_outgoing_call;
        call_poller.start();
        emit inCallChanged();
        emit outgoingCall(call_num);
    }
    return dial_status.last();
}

bool QiTalkAPI::getInCall() const {
    return call_status == in_call;
}

/* MISC */

void QiTalkAPI::pollShortMeta()
{
    if (sending_sms) return;
    // Get Signal Strength
    const QVector<QString> read_data = request(cmd.signal_strength);
    if (read_data.last() != "OK") {
        return;
    }
    signal_strength = read_data[1].split(" ").at(1).split(",").at(0).toInt() * 100 / 30; // Convert to %
    emit signalStrengthChanged();
    updateNetworkConfig();
}

void QiTalkAPI::pollLongMeta()
{
    if (sending_sms) return;
    updateImei();
    const QVector<QString> manu_data(request(cmd.manufacturer)), mod_data(request(cmd.modem_model));
    if (manu_data.last() != "OK" || mod_data.last() != "OK" || manu_data.size() < 2 || mod_data.size() < 2) {
        setModemMake("ERROR");
        return;
    }
    setModemMake(tr("%1 %2").arg(manu_data[1]).arg(mod_data[1]));
}

// NETWORK DETAILS
void QiTalkAPI::updateNetworkConfig()
{
    const QVector<QString> net_config_data = request(cmd.net_config);
    const QRegularExpressionMatch net_config_captured = regex.network_details.match(net_config_data[1]);
    if (!net_config_captured.hasMatch()) {
        qWarning() << "Error getting config name. Printing request:";
        for (int i = 0, sz = net_config_data.size(); i < sz; ++i) {
            qWarning() << tr("    net_config_data[%1]: %2").arg(i).arg(net_config_data[i]);
        }
        setNetworkName("ERROR REQUESTING INFO: " + net_config_data.last());
        return;
    }
    const QString mode = net_config_captured.captured(1);
    if (mode == "0") {
       setNetworkMode("Automatic Mode");
    } else if (mode == "1") {
       setNetworkMode("Manual Mode");
    } else if (mode == "2") {
       setNetworkMode("Manual Deregister");
    } else if (mode == "3") {
       setNetworkMode("Manual/Automatic Mode");
    } else {
        qWarning() << "Error parsing network config info";
        setNetworkMode("Unknown: " + mode);
    }

    const QString access = net_config_captured.captured(4);
    if (access == "0") {
        setNetworkAccess("GSM");
    } else if (access == "2") {
        setNetworkAccess("UTRAN");
    } else if (access == "3") {
        setNetworkAccess("GSM W/EGPRS");
    } else if (access == "4") {
        setNetworkAccess("UTRAN W/HSDPA");
    } else {
        qWarning() << "Error parsing network access info";
        setNetworkAccess("Unknown: " + access);
    }

    setNetworkName(net_config_captured.captured(3));
}

void QiTalkAPI::updateImei()
{
    const QVector<QString> imei_data = request(cmd.imei);
    if (imei_data.last() != "OK" || imei_data.size() < 2) {
        return;
    }
    imei = request(cmd.imei)[1];
    emit imeiChanged();
}

int QiTalkAPI::getSignalStrength() const
{
    return signal_strength;
}

QString QiTalkAPI::getImei() const
{
    return imei;
}

void QiTalkAPI::setNetworkName(const QString new_network_name)
{
    if (new_network_name == network_name) {
        return;
    }
    network_name = new_network_name;
    emit networkNameChanged();
}
QString QiTalkAPI::getNetworkName() const
{
    return network_name;
}

void QiTalkAPI::setNetworkMode(const QString new_network_mode)
{
    if (new_network_mode == network_mode) {
        return;
    }
    network_mode = new_network_mode;
    emit networkModeChanged();
}
QString QiTalkAPI::getNetworkMode() const
{
    return network_mode;
}

void QiTalkAPI::setNetworkAccess(const QString new_network_access)
{
    if (new_network_access == network_access) {
        return;
    }
    network_access = new_network_access;
    emit networkAccessChanged();
}
QString QiTalkAPI::getNetworkAccess() const
{
    return network_access;
}

// HARDWARE

void QiTalkAPI::setModemMake(const QString new_modem_make)
{
    if (new_modem_make == modem_make) {
        return;
    }
    modem_make = new_modem_make;
    emit modemMakeChanged();
}
QString QiTalkAPI::getModemMake() const
{
    return modem_make;
}

void QiTalkAPI::setPower(const bool new_power_on)
{
    power_on = new_power_on;
    if (power_on) {
        if (!open()) {
            qWarning("QiTalkAPI::setPower(): Polling port %s", qPrintable(serial.portName()));
            setPower(false);
            return;
        }
        if (power_poller.isActive())
            power_poller.stop();
        emit powerUp();
    } else {
        close();
        if (!power_poller.isActive())
            power_poller.start();
        emit powerDown();
    }
    emit powerChanged();
}

bool QiTalkAPI::getPower() const
{
    return power_on;
}

void QiTalkAPI::pollPower()
{
    setPower(true);
}

// SETTINGS

void QiTalkAPI::setInputVolume(const int new_input_volume)
{
    if (new_input_volume == input_volume) {
        return;
    }
    input_volume = new_input_volume;
    if (request(cmd.set_input_volume.arg( input_volume * input_volume_coeff )).last() != "OK") {
        qWarning() << "Failed to set mic volume";
    }

    emit inputVolumeChanged();
}

int QiTalkAPI::getInputVolume() const
{
    return input_volume;
}

void QiTalkAPI::setOutputVolume(const int new_output_volume)
{
    if (new_output_volume == output_volume) {
        return;
    }
    if (request(cmd.set_output_volume.arg(new_output_volume*655)).last() != "OK") {
        qWarning() << "Failed to set mic volume";
        return;
    }
    output_volume = new_output_volume;
    emit outputVolumeChanged();
}

int QiTalkAPI::getOutputVolume() const
{
    return output_volume;
}

void QiTalkAPI::setAudioMode(const bool new_audio_mode)
{
    if (new_audio_mode == audio_mode) {
        return;
    }
    if (request(cmd.set_output_path.arg(int(new_audio_mode))).last() != "OK") {
        qWarning() << "Failed to set audio mode";
        return;
    }
    audio_mode = new_audio_mode;
    emit audioModeChanged();
}

bool QiTalkAPI::getAudioMode() const
{
    return audio_mode;
}

bool QiTalkAPI::getJackMode() const
{
    return true;
}

bool QiTalkAPI::getSpeakerMode() const
{
    return false;
}

QString QiTalkAPI::getPortName() const
{
    return serial.portName();
}

void QiTalkAPI::setPortName(const QString new_port_name)
{
    if (new_port_name == serial.portName())
        return;
    if (call_status == in_call) {
        qWarning() << "Cannot set the port name mid-call!";
        return;
    }
    if (power_on)
        setPower(false);
    serial.setPortName(new_port_name);
    setPower(true);
    emit portNameChanged();
}

/* LOW LEVEL SERIAL FUNCTIONS */

void QiTalkAPI::close()
{
    qDebug() << "Closing port" << getPortName();
    expect_response = false;
    stop_meta_polling();
    if (serial.isOpen())
        serial.close();
}

bool QiTalkAPI::open()
{
    serial.setBaudRate(QSerialPort::Baud115200);
    if (!serial.isOpen()) {
        if (!serial.open(QIODevice::ReadWrite)) {
            qWarning() << "    QiTalkAPI::open(): Failed opening serial:" << serial.errorString();
            return false;
        }
    }

    if (request(cmd.test).last() != "OK") {
        qWarning() << "    QiTalkAPI::open(): Test Command failed!";
        return false;
    }

    const QVector<QString> config_commands = {
        cmd.restore_defaults,
        cmd.caller_id,
        cmd.message_format,
        cmd.interfacing_ports,
        cmd.no_automatic_answering,
        cmd.call_forwarding,
        cmd.delete_sms.arg("1,4"),
        cmd.ringtone_mute.arg("0"),
        cmd.ringtone_vol.arg("7"),
        cmd.ringtone_output.arg("0")
    };

    // Default Settings (TODO: Maybe make a .conf file for this?
    setInputVolume(15);
    setOutputVolume(50);

    for (int i = 0, sz = config_commands.size(); i < sz; ++i) {
        if (request(config_commands[i]).last() != "OK") {
            qWarning() << "    QiTalkAPI::open(): failed to execute" << config_commands[i];
        }
    }

    qDebug() << "QiTalk is Initialized";
    pollShortMeta();
    pollLongMeta();
    start_meta_polling();
    return true;
}

void QiTalkAPI::start_meta_polling()
{
    short_meta_poller.start();
    long_meta_poller.start();
}

void QiTalkAPI::stop_meta_polling()
{
    short_meta_poller.stop();
    long_meta_poller.stop();
}

bool QiTalkAPI::isWrongEncoding(const QString sms_body)
{
    return regex.wrong_encoding.match(sms_body).hasMatch();
}

bool QiTalkAPI::validNum(QString num) const
{
    const bool status = regex.valid_num.match(num).hasMatch();
    if (!status) {
        qWarning() << "QiTalkAPI::validNum(): Invalid num:" << num;
    }
    return status;
}

QString QiTalkAPI::toInternational(const QString num_in)
{
    ////TODO: Internationalize once UK-Only code is sorted out
    QString num = num_in.startsWith("0")?  "+44" + num_in.right(num_in.size()-1) : num_in;
    qDebug() << "QiTalk::toInternational():" << num_in << "->" << num;
    return num;
}

QString QiTalkAPI::hexStringToUtf16(const QString strin)
{
    const QString BOM = "FEFF";
    return QString::fromUtf16(reinterpret_cast<const char16_t*>(QByteArray::fromHex((BOM + strin).toLocal8Bit()).constData()));
}

QByteArray QiTalkAPI::utf16ToHexString(QString strin)
{
    strin.prepend(QChar::ByteOrderMark);
    return QByteArray::fromRawData(reinterpret_cast<const char*>(strin.constData()), (strin.size()+1)*2).toHex();
}

void QiTalkAPI::handleReadyRead()
{
    if (expect_response) return;

    const QVector<QString> read_data = readAll();

    for (int i = 0, sz = read_data.size(); i < sz; ++i) {
        qDebug() << QString("read_data[%1]: %2").arg(i).arg(read_data[i]);
    }

    // TODO: Have this deal with incoming calls
    if (isSMS(read_data)) {
        handleIncomingSMS(read_data);
    } else if (isCall(read_data)) {
        handleIncomingCall(read_data);
    }
}

bool QiTalkAPI::write(const QString write_data)
{
    if (!(power_on || write_data == "AT")) {
        qWarning() << "Not powered on - not writing:" << write_data;
        return false;
    }
    const QByteArray write_encoded = encode(write_data + term);
    const qint64 write_bytes = serial.write(write_encoded);
    serial.flush();
    if (write_bytes != write_encoded.size()) {
        qWarning() << "Warn: not all data sent!";
        return false;
    }
    emit written(write_data);
    return true;
}

QVector<QString> QiTalkAPI::request(const QString write_data, const bool is_sms, const int timeout_duration)
{
    QDebug deb = qDebug();
    deb << qPrintable(tr("%1...\t").arg(write_data));
    expect_response = true;
    if (!write(write_data)) {
        expect_response = false;
        return QVector<QString>({write_data, "ERROR WRITING"});
    }

    if (!serial.waitForReadyRead(timeout_duration)) {
        qWarning() << "TIMEOUT";
        expect_response = false;
        return QVector<QString>({write_data, "TIMEOUT"});
    }
    QVector<QString> read_data = readAll(is_sms);
    if (read_data.first() != write_data) {
        read_data.prepend(write_data);
    }
    deb << qPrintable(read_data.last());
    expect_response = false;
    return read_data;
}

QVector<QString> QiTalkAPI::readAll(const bool is_sms)
{
    QByteArray read_encoded = serial.readAll();
    /* Make sure ALL the data comes through */
    if (is_sms) {
        while (serial.waitForReadyRead(50)) {
            read_encoded += serial.readAll();
        }
    } else {
        while (serial.waitForReadyRead(50) || !decode(read_encoded).endsWith(term)) {
            read_encoded += serial.readAll();
        }
    }
    QVector<QString> read_data = decode(read_encoded).split(term).toVector();
    if (read_data.first().isEmpty()) {
        read_data.removeFirst();
    }
    read_data.first().replace("\r", "");
    if (read_data.last().isEmpty()) {
        read_data.removeLast();
    }
    for (int i = 0, sz = read_data.size(); i < sz; ++i) {
        if (read_data[i] == "POWERED DOWN") {
            setPower(false);
            return QVector<QString>({"POWERED DOWN"});
        }
    }
    emit read(read_data);
    return read_data;
}

QString QiTalkAPI::decode(const QByteArray read_encoded)
{
    return QString::fromUtf8(read_encoded);
}

QByteArray QiTalkAPI::encode(const QString write_decoded)
{
    return write_decoded.toUtf8();
}
