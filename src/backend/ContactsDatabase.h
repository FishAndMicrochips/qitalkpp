/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#ifndef CONTACTSDATABASE_H
#define CONTACTSDATABASE_H

#include <QObject>
#include <QtSql/QSqlTableModel>
#include <QStandardPaths>

class ContactsDatabase : public QSqlTableModel
{
    Q_OBJECT
    //Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    //Q_PROPERTY(QString num READ num WRITE setNum NOTIFY numChanged)
public:
    ContactsDatabase(QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;


    Q_INVOKABLE QString defaultName() const { return default_name; }
    Q_INVOKABLE QString defaultNum () const { return default_num;  }
    Q_INVOKABLE QString defaultIcon() const { return default_icon; }

    Q_INVOKABLE QString name(const QString id, const QString index = "id") const;
    Q_INVOKABLE bool setName(const QString id, const QString name);

    Q_INVOKABLE QString num(const QString id) const;
    Q_INVOKABLE bool setNum(const QString id, const QString num);

    Q_INVOKABLE QString getId(const QString num) const;

    Q_INVOKABLE bool setIcon(const QString id, const QString icon_path);

    Q_INVOKABLE bool deleteContact(const QString id);
    Q_INVOKABLE void editRequest(const QString contact_id, const QString new_name, const QString old_name, const QString new_num, const QString old_num);

public slots:
    Q_INVOKABLE void newContact(const QString num = "<Insert Num>");
signals:
    void nameChanged();
    void numChanged(const QString name);
private:
    const QString default_name = "<Insert Name>";
    const QString default_num = "<Insert Num>";
    const QString default_icon = "file://" + QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/icons/default.png";

    const QString re_phonenum = "^\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,14}$";
    const QString re_name = "^\\w+$";
    const QString re_id = "^\\d+$";

    bool setGeneral(const QString field, const QString id, const QString value, const QString index = "id");
    QString getGeneral(const QString field, const QString id, const QString index = "id") const;

};

#endif // CONTACTSDATABASE_H
