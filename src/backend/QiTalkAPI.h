#ifndef QITALKAPI_H
#define QITALKAPI_H

#include <QObject>
#include <QRegularExpression>
#include <QDateTime>
#include <QSerialPort>
#include <QTimer>

class QiTalkAPI : public QObject
{
    Q_OBJECT
    /* Calls */
    Q_PROPERTY(QString call_num READ getCallNum NOTIFY callNumChanged)
    Q_PROPERTY(bool in_call READ getInCall NOTIFY inCallChanged)
    /* Texts */
    Q_PROPERTY(QString text_num READ getTextNum NOTIFY textNumChanged)
    Q_PROPERTY(QString text_body READ getTextBody NOTIFY textBodyChanged)
    Q_PROPERTY(QString text_time READ getTextTime NOTIFY textTimeChanged)
    /* Settings*/
    Q_PROPERTY(int input_volume READ getInputVolume WRITE setInputVolume NOTIFY inputVolumeChanged)
    Q_PROPERTY(int output_volume READ getOutputVolume WRITE setOutputVolume NOTIFY outputVolumeChanged)
    Q_PROPERTY(bool audio_mode READ getAudioMode WRITE setAudioMode NOTIFY audioModeChanged)
    Q_PROPERTY(bool speaker_mode READ getSpeakerMode NOTIFY audioModeChanged)
    Q_PROPERTY(bool jack_mode READ getJackMode NOTIFY audioModeChanged)
    /* Serial */
    Q_PROPERTY(QString port_name READ getPortName WRITE setPortName NOTIFY portNameChanged)
    /* Network Info */
    Q_PROPERTY(int signal_strength READ getSignalStrength NOTIFY signalStrengthChanged)
    Q_PROPERTY(QString imei READ getImei NOTIFY imeiChanged)
    Q_PROPERTY(QString network_name READ getNetworkName NOTIFY networkNameChanged)
    Q_PROPERTY(QString network_mode READ getNetworkMode NOTIFY networkModeChanged)
    Q_PROPERTY(QString network_access READ getNetworkAccess NOTIFY networkAccessChanged)
    /* Hardware Info */
    Q_PROPERTY(QString modem_make READ getModemMake NOTIFY modemMakeChanged)
    Q_PROPERTY(bool power_on READ getPower WRITE setPower NOTIFY powerChanged)
public:
    explicit QiTalkAPI(QObject *parent = nullptr, const QString port_name = "/dev/ttyS0");
    ~QiTalkAPI();
    // SMS
    Q_INVOKABLE QString sendSMS(const QString num, const QString body);

    // CALLS
    Q_INVOKABLE QString dial(const QString num);
    Q_INVOKABLE QString dialInCall(const QString num);
    Q_INVOKABLE QString hangup();
    Q_INVOKABLE QString connectCall();

    // MISC
    bool validNum(QString num) const;

    // SERIAL INTERFACING
    bool open();
    void close();
    Q_INVOKABLE bool write(const QString write_data);
    QVector<QString> request(const QString write_data, const bool is_sms = false, const int timeout_duration = 400);
    QVector<QString> readAll(const bool is_sms = false);

signals:
    //SMS
    void smsReceived(const QString num, const QString body, const QString datetime_str);
    void smsSent    (const QString num, const QString body, const QString datetime_str);
    void textNumChanged();
    void textBodyChanged();
    void textTimeChanged();

    //CALLS
    void outgoingCall(const QString num);
    void incomingCall(const QString num);
    void callStarted (const QString num);
    void callEnded   (const QString num);
    void callNumChanged();
    void inCallChanged();

    // NETWORK DETAILS
    void signalStrengthChanged();
    void imeiChanged();
    void networkNameChanged();
    void networkModeChanged();
    void networkAccessChanged();

    // HARDWARE
    void modemMakeChanged();
    void powerChanged();
    void powerUp();
    void powerDown();

    // SETTINGS
    void inputVolumeChanged();
    void outputVolumeChanged();
    void audioModeChanged();
    void portNameChanged();

    // SERIAL INTERFACING
    void written(const QString write_data);
    void read(const QVector<QString> read_data);

private:
    // SMS
    bool sending_sms = false;
    void handleIncomingSMS(const QVector<QString> read_data);
    bool isSMS(const QVector<QString> read_data);
    QString text_num = "";
    QString text_body = "";
    QString text_time = "";
    QString getTextNum() const;
    void setTextNum(const QString new_text_num);
    QString getTextBody() const;
    void setTextBody(const QString new_text_body);
    QString getTextTime() const;
    void setTextTime(const QString new_text_time);

    // CALLS
    void handleIncomingCall(const QVector<QString> read_data);
    bool isCall(const QVector<QString> read_data);
    QTimer call_poller;
    QString call_num = "";
    QString getCallNum() const;
    bool getInCall() const;

    // LOW LEVEL SERIAL INTERFACING
    /// The Basics

    /// Encoding for serial
    QString decode(const QByteArray read_encoded);
    QByteArray encode(const QString write_decoded);

    /// Get emoji to receive properly
    QString hexStringToUtf16(const QString strin);
    QByteArray utf16ToHexString(QString strin);
    bool isWrongEncoding(const QString sms_body);

    QSerialPort serial;
    bool expect_response = false;
    const QString term = "\r\n";
    const QString sms_term = "\u001A";

    //MISC
    QTimer short_meta_poller;
    QTimer long_meta_poller;
    QTimer power_poller;

    // NETWORK DETAILS
    int signal_strength = 0;
    int getSignalStrength() const;

    QString imei = "";
    QString getImei() const;
    void updateImei();

    QString network_name = "Unknown";
    QString getNetworkName() const;
    void setNetworkName(const QString new_network_name);

    QString network_mode = "Unknown";
    QString getNetworkMode() const;
    void setNetworkMode(const QString new_network_mode);

    QString network_access = "Unknown";
    QString getNetworkAccess() const;
    void setNetworkAccess(const QString new_network_access);

    void updateNetworkConfig();

    // HARDWARE
    QString modem_make = "Unknown";
    QString getModemMake() const;
    void setModemMake(const QString new_modem_make);

    bool power_on = true;
    void setPower(const bool new_power_on);
    bool getPower() const;

    // TIMER CONTROL
    void start_meta_polling();
    void stop_meta_polling();

    // SETTINGS
    const int input_volume_coeff = 100; // Acquired through trial and error
    int input_volume;
    void setInputVolume(const int new_input_volume);
    int getInputVolume() const;

    int output_volume;
    void setOutputVolume(const int new_output_volume);
    int getOutputVolume() const;

    bool audio_mode;
    void setAudioMode(const bool new_audio_mode);
    bool getAudioMode() const;
    bool getJackMode() const;
    bool getSpeakerMode() const;

    QString getPortName() const;
    void setPortName(const QString new_port_name);

    struct { /* Regexes to compare key data to */
        // MISC
        //const QString valid_num_string = "(\\+|0)(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,14}";
        const QString valid_num_string = "\\+?[\\d\\s]+";
        QRegularExpression error       = QRegularExpression("^\\+(CM[ES]) ERROR: (\\d+)$");
        QRegularExpression valid_num   = QRegularExpression(QString("^(%1)$").arg(valid_num_string));
        QRegularExpression wrong_encoding = QRegularExpression("^(0?[\\dA-F]{4})+$");

        // SMS
        QRegularExpression new_sms     = QRegularExpression("^\\+CMTI:\\s*\"([^\"]+)\",(\\d+).*$");
        QRegularExpression sms_details = QRegularExpression("^\\+CMGR:\\s*\"([^\"]+)\",\"([^\"]+)\",[^,]*,\"([^\"]+)\".*$");

        // CALLS
        QRegularExpression new_call     = QRegularExpression("^RING$");
        QRegularExpression call_details = QRegularExpression(QString("^\\+CLIP:\\s*\"(%1)\",(\\d+).*$").arg(valid_num_string));
        QRegularExpression call_poll    = QRegularExpression("^\\+CPAS:\\s*(\\d).*$");

        // MISC
        QRegularExpression network_details = QRegularExpression("^\\+COPS:\\s*(\\d+),(\\d+),\"([A-Za-z0-9 ]+)\",(\\d+).*$");
    } regex;

    struct commands {
        // CONFIGURATION
        const QString test = "AT";
        const QString restore_defaults = "AT&F";
        const QString caller_id = "AT+CLIP=1";
        const QString message_format = "AT+CMGF=1";
        const QString interfacing_ports = "AT+QURCCFG=\"urcport\",\"uart1\"";
        const QString no_automatic_answering = "ATS0=0";
        const QString call_forwarding = "AT+CCFC=0,4";

        // CALLS
        const QString dial = "ATD%1;";
        const QString connect = "ATA";
        const QString hangup = "ATH";
        const QString call_data = "AT+CPAS";
        const QString dial_in_call = "AT+VTS=%1";

        // SETTINGS
        const QString ringtone_vol = "AT+CRSL=%1";
        const QString ringtone_mute = "AT+CALM=%1";
        const QString ringtone_output = "AT+QRCH=%1";
        const QString set_input_volume = "AT+QMIC=1,%1,%1";
        const QString set_output_volume = "AT+QRXGAIN=0,%1,%1";
        const QString set_output_path = "AT+QAUDPATH=%1";

        // DIAGNOSTICS
        const QString signal_strength = "AT+CSQ";
        const QString manufacturer = "AT+CGMI";
        const QString modem_model = "AT+CGMM";
        const QString net_config = "AT+COPS?";
        const QString imei = "AT+CGSN";

        // SMS
        const QString send_sms = "AT+CMGS=\"%1\"";
        const QString receive_sms = "AT+CMGR=%1";
        const QString delete_sms = "AT+CMGD=%1";
    } cmd;

    QString toInternational(QString num); // Turns a regional phone num into an international format
    enum {not_in_call, ring_incoming_call, ring_outgoing_call, in_call} call_status;
private slots:
    void handleReadyRead();
    void pollCallStatus();
    void pollShortMeta();
    void pollLongMeta();
    void pollPower();
};

#endif // QITALKAPI_H
