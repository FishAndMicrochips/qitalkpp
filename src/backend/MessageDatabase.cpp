/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#include "MessageDatabase.h"
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtSql/QSqlError>
#include <QDebug>

static const char *conversationsTableName = "Conversations";

static void createTable()
{
    if (QSqlDatabase::database().tables().contains(conversationsTableName)) return;
    QSqlQuery q;
    if (!q.exec(
        "CREATE TABLE IF NOT EXISTS 'Conversations' ("
        "   'contact_id' TEXT NOT NULL,"
        "   'by_me' TINYINT,"
        "   'timestamp' TEXT NOT NULL,"
        "   'message' TEXT NOT NULL,"
        "   FOREIGN KEY('contact_id') REFERENCES Contacts ( id )"
        ")")) {
        qFatal("Failed to query database: %s", qPrintable(q.lastError().text()));
    }
}

MessageDatabase::MessageDatabase(QObject *parent): QSqlTableModel(parent)
{
    createTable();
    setTable(conversationsTableName);
    setSort(2, Qt::DescendingOrder);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

QString MessageDatabase::recipient() const { return m_contact_id; }

void MessageDatabase::setRecipient(const QString &recipient)
{
    if (recipient == m_contact_id)
        return;

    m_contact_id = recipient;
    const QString filterString = QString::fromLatin1("contact_id = '%1'").arg(m_contact_id);
    setFilter(filterString);
    select();

    emit recipientChanged();
}

QVariant MessageDatabase::data(const QModelIndex &index, int role) const
{
    return role < Qt::UserRole?
                QSqlTableModel::data(index, role):
                record(index.row()).value(role - Qt::UserRole);
}

QHash<int, QByteArray> MessageDatabase::roleNames() const
{
    QHash<int, QByteArray> names;

    names[Qt::UserRole + 0] = "contact_id";
    names[Qt::UserRole + 1] = "by_me";
    names[Qt::UserRole + 2] = "timestamp";
    names[Qt::UserRole + 3] = "message";
    return names;
}

void MessageDatabase::append_by_contact_id(const QString contact_id, const int by_me, const QString message, const QString timestamp)
{
    QSqlRecord newMessage = record();
    newMessage.setValue("contact_id", contact_id);
    newMessage.setValue("by_me", by_me);
    newMessage.setValue("timestamp", timestamp);
    newMessage.setValue("message", message);

    if (!insertRecord(rowCount(), newMessage))
        qWarning() << "Failed to send message:" << lastError().text();
    else if (!submitAll())
        qWarning() << "Failed to submit new message to database:" << lastError().text();
}

void MessageDatabase::append_by_num(const QString num, const int by_me, const QString message, const QString timestamp)
{
    QString contact_id = "";
    QSqlQuery q;
    if (!q.exec( tr("SELECT id FROM Contacts WHERE num = '%1'").arg(num))) {
        qWarning() << "Failed to retriever record for recieveMessage";
        return;
    }

    if (q.next()) {
        QString contact_id = q.value(0).toString();
        append_by_contact_id(contact_id, by_me, message, timestamp);
        while (q.next()) {
            contact_id = q.value(0).toString();
            append_by_contact_id(contact_id, by_me, message, timestamp);
        }
    } else {
        qDebug() << "This contact doesn't yet exist!";
        // This is a bit of a spicie hack but it works: recurse until the contact exists.
        emit newContact(num);
        append_by_num(num, by_me, message, timestamp);
    }

}

void MessageDatabase::sendMessage(const QString num, const QString message, const QString timestamp)
{
    append_by_num(num, is_by_me, message, timestamp);
}

void MessageDatabase::receiveMessage(const QString num, const QString message, const QString timestamp)
{
    append_by_num(num, is_not_by_me, message, timestamp);
}

void MessageDatabase::neutralMessage(const QString num, const QString message, const QString timestamp)
{
    append_by_num(num, neutral_by_me, message, timestamp);
}

void MessageDatabase::incomingCall(const QString num)
{
    if (call_status == ring_incoming_call) {
        return;
    }
    call_status = ring_incoming_call;
    receiveMessage(num, "<Attempting To Call You>");
}

void MessageDatabase::outgoingCall(const QString contact_id)
{
    if (call_status == ring_outgoing_call) {
        return;
    }
    call_status = ring_outgoing_call;
    sendMessage(contact_id, "<Attempting a call>");
}

void MessageDatabase::callStarted(const QString num)
{
    if (call_status == in_call) {
        return;
    }
    call_status = in_call;
    neutralMessage(num, "<Call started>");
}

void MessageDatabase::callEnded(const QString num)
{
    switch (call_status)
    {
    case not_in_call:
        qWarning() << "Ending a call despite no status?";
        break;
    case in_call:
        neutralMessage(num, "<Call Ended>");
        break;
    case ring_incoming_call:
        receiveMessage(num, "<Call Attempt Ended>");
        break;
    case ring_outgoing_call:
        sendMessage(num, "<Call Attempt Ended>");
    }
    call_status = not_in_call;
}

void MessageDatabase::command_written(const QString write_data)
{
    sendMessage("CONSOLE", write_data);
}

void MessageDatabase::command_received(const QVector<QString> read_data)
{
    QString read_data_joined = "";
    for (int i = 0, sz = read_data.size(); i < sz; ++i) {
        read_data_joined.append(read_data[i] + "\n");
    }
    receiveMessage("CONSOLE", read_data_joined);
}
