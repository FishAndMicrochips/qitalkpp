/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#include "ContactsDatabase.h"
#include <QDebug>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QRandomGenerator>

static void createTable()
{
    if (QSqlDatabase::database().tables().contains(QStringLiteral("Contacts"))) return;

    QSqlQuery q;
    if (!q.exec("CREATE TABLE IF NOT EXISTS 'Contacts' ("
                "   'id'   TEXT NOT NULL," // Used as a primary database key
                "   'name' TEXT NOT NULL," // Name :)
                "   'num'  TEXT NOT NULL," // Used to call and text
                "   'icon' TEXT NOT NULL,"  // Path to chat icon
                "   PRIMARY KEY(id)"
                ")"))
        qFatal("Failed to query database: %s", qPrintable(q.lastError().text()));

}

ContactsDatabase::ContactsDatabase(QObject *parent): QSqlTableModel(parent)
{
    createTable();
    setTable("Contacts");
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    //setFilter("num <> 'CONSOLE'");
    select();
}

QVariant ContactsDatabase::data(const QModelIndex &index, int role) const
{
    return role < Qt::UserRole?
                QSqlTableModel::data(index, role):
                record(index.row()).value(role - Qt::UserRole);
}

QHash<int, QByteArray> ContactsDatabase::roleNames() const
{
    QHash<int, QByteArray> role_names;

    role_names[Qt::UserRole] = "id";
    role_names[Qt::UserRole + 1] = "name";
    role_names[Qt::UserRole + 2] = "num";
    role_names[Qt::UserRole + 3] = "icon";

    return role_names;
}


bool ContactsDatabase::setGeneral(const QString field, const QString id, const QString value, const QString index)
{
    const QString cmd = QString("UPDATE Contacts SET %1 = '%2' WHERE %4 = '%3'").arg(field).arg(value).arg(id).arg(index);
    QSqlQuery q;
    if (!q.exec(cmd)) {
        qWarning() << "Failed to update" << field << "to" << value << "." << q.lastError().text();
        return false;
    }
    submitAll();
    return true;
}

QString ContactsDatabase::getGeneral(const QString field, const QString id, const QString index) const
{
    const QString cmd = QString("SELECT %1 FROM Contacts WHERE %2 = '%3'").arg(field).arg(index).arg(id);
    QSqlQuery q;
    if (!q.exec(cmd)) {
        qWarning() << "Failed to execute SELECT:" << q.lastError().text();
        return "ERROR";
    }
    if (!q.next()) {
        qWarning() << "No record with" << index << id;
        return QString("<Insert %1>").arg(field);
    }
    const QString value = q.value(0).toString();
    return value;
}

bool ContactsDatabase::setIcon(const QString id, const QString icon_path)
{
    return setGeneral("icon", id, icon_path);
}

QString ContactsDatabase::name(const QString id, const QString index) const
{
    return getGeneral("name", id, index);
}

bool ContactsDatabase::setName(const QString id, const QString name)
{
    return setGeneral("name", id, name);
}

QString ContactsDatabase::num(const QString id) const
{
    return getGeneral("num", id);
}

bool ContactsDatabase::setNum(const QString id, const QString num)
{
    return setGeneral("num", id, num);
}

QString ContactsDatabase::getId(const QString num) const
{
    return getGeneral("id", num, "num");
}

void ContactsDatabase::newContact(const QString num)
{
    QSqlQuery q;
    if (!q.exec(QString("INSERT INTO Contacts VALUES ('%1', '%2', '%3', '%4')")
                .arg(QString::number(QRandomGenerator::securelySeeded().generate64()))
                .arg(num == default_num? default_name : num)
                .arg(num)
                .arg(default_icon))) {
        qWarning() << "Failed to create contact: |" << q.lastError().text();
        return;
    }

    if (!submitAll()) {
        qWarning() << "Failed to submit new contact to database:" << lastError().text();
    }
}

bool ContactsDatabase::deleteContact(const QString id)
{
    QString cmd = QString("DELETE FROM Contacts WHERE id = '%1'").arg(id);
    QSqlQuery q;
    if (!q.exec(cmd)) {
        qWarning() << "Failed to delete row from the table";
        return false;
    }
    submitAll();
    return true;
}

void ContactsDatabase::editRequest(const QString contact_id, const QString new_name, const QString old_name, const QString new_num, const QString old_num)
{
    if ((new_name.length() > 0) && (new_name != old_name)) {
        setName(contact_id, new_name);
    }

    if ((new_num.length() > 0) && (new_num != old_num)) {
        setNum(contact_id, new_num);
    }
}
