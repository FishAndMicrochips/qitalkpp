/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#ifndef MESSAGEDATABASE_H
#define MESSAGEDATABASE_H

#include <QObject>
#include <QSqlTableModel>
#include <QDateTime>

class MessageDatabase : public QSqlTableModel
{
    Q_OBJECT
    Q_PROPERTY(QString recipient READ recipient WRITE setRecipient NOTIFY recipientChanged)
public:
    MessageDatabase(QObject *parent = nullptr);

    QString recipient() const;
    void setRecipient(const QString &recipient);

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

public slots:
    // GENERAL APPENDING
    Q_INVOKABLE void sendMessage   (const QString num, const QString message, const QString timestamp = QDateTime::currentDateTime().toString(Qt::ISODate));
    Q_INVOKABLE void receiveMessage(const QString num, const QString message, const QString timestamp = QDateTime::currentDateTime().toString(Qt::ISODate));
    Q_INVOKABLE void neutralMessage(const QString num, const QString message, const QString timestamp = QDateTime::currentDateTime().toString(Qt::ISODate));
    // CALLS
    Q_INVOKABLE void incomingCall(const QString num);
    Q_INVOKABLE void outgoingCall(const QString num);
    Q_INVOKABLE void callStarted (const QString num);
    Q_INVOKABLE void callEnded   (const QString num);

    void command_written(const QString write_data);
    void command_received(const QVector<QString> read_data);
signals:
    void recipientChanged();
    void newContact(const QString num);

private:
    QString m_contact_id;
    void append_by_contact_id(const QString contact_id, const int by_me, const QString message, const QString timestamp = QDateTime::currentDateTime().toString(Qt::ISODate));
    void append_by_num       (const QString num,        const int by_me, const QString message, const QString timestamp = QDateTime::currentDateTime().toString(Qt::ISODate));

    const int is_not_by_me = 0;
    const int is_by_me = 1;
    const int neutral_by_me = 2;
    enum {not_in_call, ring_incoming_call, ring_outgoing_call, in_call} call_status;
};

#endif // MESSAGEDATABASE_H
