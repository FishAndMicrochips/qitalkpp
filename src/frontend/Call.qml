/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import fishandmicrochips.qitalk.api 1.0

Page {
    id: root

    property int secs : 0
    property int mins : 0
    property int hours: 0

    readonly property string connecting_text: "Call connecting..."

    property string timer_text: connecting_text

    function callStarted() {
        timer_text = (hours < 10? "0" : "") + String(hours) + ":"
                   + (mins  < 10? "0" : "") + String(mins)  + ":"
                   + (secs  < 10? "0" : "") + String(secs);
        timer.start();
    }
    Component.onCompleted: {
        console.log("Call setup done");
    }

    Connections {
        target: api
        onCallStarted: callStarted();
        onCallEnded: {
            timer.stop();
            root.StackView.view.pop()
        }
    }

    Timer {
        id: timer
        interval: 1000
        running: true
        repeat: true
        onTriggered: {
            secs += 1;
            if (secs === 60) {
                secs = 0;
                mins += 1;
            }
            if (mins === 60) {
                mins = 0;
                hours += 1;
            }
            if (hours === 100) {
                hours = 0;
            }
            timer_text = (hours < 10? "0" : "") + String(hours) + ":"
                       + (mins  < 10? "0" : "") + String(mins)  + ":"
                       + (secs  < 10? "0" : "") + String(secs);
        }
    }

    header: ToolBar {
        id: toolbar
        Material.background: Material.color(Material.Shade300)
        ToolButton {
            text: "<"
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            onClicked: root.StackView.view.pop()
        }
        Label {
            id: pageTitle
            text: "☎: " + contactsDatabase.name(api.call_num, "num")
            font.pixelSize: 20
            anchors.centerIn: parent
        }
    }
    Column {
        anchors.fill: parent
        spacing: 20
        Label {
            width: parent.width
            id: call_num_label
            text: api.call_num
            font.pixelSize: 30
            height: 50
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        Label {
            width: parent.width
            id: timer_label
            text: timer_text
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        NumPad {
            id: numPad
            anchors.horizontalCenter: parent.horizontalCenter
        }
        RoundButton{
            Material.background: Material.color(Material.Shade100)
            anchors.horizontalCenter: parent.horizontalCenter
            height: 90
            width: 90
            font.pixelSize: 30
            font.bold: true
            text: "X"
            onClicked: {
                api.hangup()
                root.StackView.view.pop()
            }
        }
    }

}
