/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3

Pane {
    id: callNotifier
    visible: false

    property string name

    function endCall() {
        api.hangup();
        callNotifier.visible = false;
    }

    function startCall(){
        api.connectCall();
        callNotifier.visible = false;
    }

    Connections {
        target: api
        onIncomingCall: {
            callNotifier.visible = !api.in_call;
            const num = api.call_num;
            name = contactsDatabase.name(num, "num");
            if (name === "<Insert name>") {
                name = num;
            }
        }
        onCallEnded: {
            callNotifier.visible = false;
        }
    }
    Column {
        anchors.fill: parent
        Label {
            width: parent.width
            id: callLabel
            text: "☎ " + name + "?"
            height: callLabel.font.pixelSize*3
            font.pixelSize: 20
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Row {
            width: parent.width
            RoundButton {
                id: noButton
                text: "X"
                font.bold: true
                font.pixelSize: noButton.height/3
                width: parent.width/2
                Material.background: Material.color(Material.Red)
                onClicked: endCall()
            }
            RoundButton{
                id: yesButton
                text: "✔️"
                font.bold: true
                font.pixelSize: yesButton.height/3
                width: parent.width/2
                Material.background: Material.color(Material.Green)
                onClicked: startCall()
            }
        }
    }
}
