/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import fishandmicrochips.qitalk.api 1.0

Column {
    id: diagnosticsLay
    anchors.fill: parent
    spacing: 20
    Label {
        font.pixelSize: 30
        width: parent.width
        height: font.pixelSize * 2
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Diagnostics"
    }
    DiagnosticsLabel {
        text: "Signal Strength: " + api.signal_strength + "%"
    }
    DiagnosticsLabel {
        text: "Modem:\n" + api.modem_make
    }
    DiagnosticsLabel {
        text: "IMEI:\n" + api.imei
    }
    DiagnosticsLabel {
        text: "Network Name:\n" + api.network_name
    }
    DiagnosticsLabel {
        text: "Network Mode:\n" + api.network_mode
    }
    DiagnosticsLabel {
        text: "Network Access Technology:\n" + api.network_access
    }
}

