/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

RoundButton {
    id: appButton
    readonly property int buttonSize: 90
    property string urlToPush
    width:  grid.buttonSize
    height: grid.buttonSize
    Layout.preferredHeight: grid.buttonSize
    Layout.preferredWidth:  grid.buttonSize
    Layout.alignment: Qt.AlignCenter
    font.pixelSize: grid.buttonSize/(3*appButton.text.length)
    onClicked: root.StackView.view.push ( urlToPush,{});
}
