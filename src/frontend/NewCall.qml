/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import fishandmicrochips.qitalk.api 1.0

Page {
    id: root
    header: ToolBar {
        id: toolbar
        Material.background: Material.color(Material.Light)
        ToolButton {
            text: "<"
            anchors.left: parent.left
            anchors.leftMargin: 10
            font.pixelSize: 20
            font.bold: true
            anchors.verticalCenter: parent.verticalCenter
            onClicked: root.StackView.view.pop()
        }
        Label {
            id: pageTitle
            text: "☎"
            font.pixelSize: 20
            anchors.centerIn: parent
        }
    }
    Column {
        width: parent.width
        spacing: 50
        NumPad {
            id: numPad
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter
        }
        RoundButton{
            height: 90
            width: 90
            font.pixelSize: 30
            font.bold: true
            text: "☎"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                //root.StackView.view.pop();
                const status = api.dial(numPad.num);
                if (status !== "OK") {
                    numPad.placeholder_text = status;
                    numPad.num = status;
                }
            }
        }
    }
}
