/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3

Pane {
    id: textNotifier
    visible: false
    x: 0
    y: 0
    z: 99

    property string name
    property bool turned_on

    Connections {
        target: api
        onSmsReceived: {
            textNotifier.visible = turned_on;
            name = contactsDatabase.name(api.text_num, "num");
            if (name === "<Insert name>") {
                name = api.text_num;
            }
        }
    }
    Column {
        anchors.fill: parent
        Label {
            id: textLabel
            width: parent.width
            readonly property int maxlen: 32
            text: "✉ " + name + ": '" + (api.text_body.length >= maxlen? api.text_body.slice(0, maxlen-3) + "..." : api.text_body) + "'"
            wrapMode: textLabel.wrap
            height: textLabel.font.pixelSize*3
            font.pixelSize: 20
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Row {
            width: parent.width
            RoundButton {
                id: noButton
                text: "X"
                font.bold: true
                font.pixelSize: noButton.height/3
                width: parent.width/2
                Material.background: Material.color(Material.Red)
                onClicked: textNotifier.visible = false
            }
            RoundButton{
                id: yesButton
                text: "✔️"
                font.bold: true
                font.pixelSize: yesButton.height/3
                width: parent.width/2
                Material.background: Material.color(Material.Green)
            }
        }
    }
}
