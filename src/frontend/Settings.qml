/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import fishandmicrochips.qitalk.api 1.0

Column {
    id: diagnosticsLay
    anchors.fill: parent
    spacing: 20
    Label {
        font.pixelSize: 30
        width: parent.width
        height: font.pixelSize * 2
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Settings")
    }
    Label {
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 20
        text: qsTr("Microphone Volume: "+api.input_volume+"%")
        width: micVolLay.width
        verticalAlignment: Text.AlignVCenter
    }
    Row {
        id: micVolLay
        anchors.horizontalCenter: parent.horizontalCenter
        Slider {
            id: micVolSlider
            from: 0
            to: 100
            value: api.input_volume
        }
        Label {
            font.pixelSize: 20
            text: micVolSlider.value.toFixed() + "%"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        RoundButton {
            id: micDoneButton
            text: "✔️"
            Material.background: Material.color(Material.Green)
            enabled: api.input_volume !== micVolSlider.value
            onClicked: api.input_volume = micVolSlider.value
        }
    }
    Label {
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 20
        text: qsTr("Audio Volume: "+api.output_volume+"%")
        width: audVolLay.width
    }
    Row {
        id: audVolLay
        anchors.horizontalCenter: parent.horizontalCenter
        Slider {
            id: audVolSlider
            from: 0
            to: 100
            value: api.output_volume
        }
        Label {
            font.pixelSize: 20
            text: audVolSlider.value.toFixed() + "%"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        RoundButton {
            id: audDoneButton
            text: "✔️"
            Material.background: Material.color(Material.Green)
            enabled: api.output_volume !== audVolSlider.value;
            onClicked: api.output_volume = audVolSlider.value;
        }
    }
    Label {
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 20
        text: qsTr("Audio Channel: " + (api.audio_mode === api.jack_mode? "Jack Mode" : "Speaker Mode"))
        width: audVolLay.width

    }
    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        Label {
            font.pixelSize: 20
            text: "Speaker Mode"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        Switch {
            id: audioModeSwitch
            onCheckedChanged: api.audio_mode = audioModeSwitch.checked
        }
        Label {
            font.pixelSize: 20
            text: "Jack Mode"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    Label {
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 20
        text: qsTr("Serial Port Name: ")
        width: portNameLay.width
    }
    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        id: portNameLay
        TextArea {
            id: portName
            placeholderText: api.port_name
        }
        RoundButton {
            id: portDoneButton
            text: "✔️"
            Material.background: Material.color(Material.Green)
            enabled: api.port_name !== portName.text;
            onClicked: api.port_name = portName.text;
        }
    }
}

