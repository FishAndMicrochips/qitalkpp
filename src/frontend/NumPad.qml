/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import fishandmicrochips.qitalk.api 1.0

//Rectangle {
Pane {
    //color: "transparent"
    property string placeholder_text: "<Insert Num>"
    property string num: placeholder_text
    width: parent.width
    height: collay.implicitHeight
    signal keyPress(string s)

    function append(s) {
        if (num === placeholder_text) {
            num = s;
        } else {
            num += s;
        }
        api.dialInCall(s)
    }
    function backSpace() {
        num = num != placeholder_text? num.slice(0, num.length - 1):  num.length <= 1? placeholder_text : num;
    }

    Column {
        id: collay
        anchors.fill: parent
        spacing: 20
        Pane {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 20
            height: 50
            Rectangle {
                id: num_input
                anchors.left: parent.left
                width: parent.width - backspace.implicitWidth - 10
                height: 50
                color: "#1C1C1C"
                radius: 7
                Label {
                    anchors.fill: parent
                    text: num
                    color: num == placeholder_text? "darkgray" : "white"
                    font.pixelSize: 30

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    Material.background: Material.color(Material.Light)
                }
            }
            RoundButton {
                id: backspace
                text: "<"
                font.pixelSize: 30
                font.bold: true
                height: num_input.height
                width: num_input.height
                flat: true
                anchors.right: parent.right
                onClicked: backSpace()
            }
        }
        GridLayout {
            id: numpad
            columns: 3
            anchors.horizontalCenter: parent.horizontalCenter
            readonly property int buttonSize: 90
            readonly property int fontSize: buttonSize/3
            RoundButton {
                readonly property string s: "1"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "2"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "3"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "4"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "5"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "6"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "7"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "8"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "9"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "*"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "0/+"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append("0")
                onPressAndHold: append("+")
                font.pixelSize: numpad.fontSize
            }
            RoundButton {
                readonly property string s: "#"
                Layout.preferredHeight: numpad.buttonSize
                Layout.preferredWidth: numpad.buttonSize
                flat: true
                text: s
                onClicked: append(s)
                font.pixelSize: numpad.fontSize
            }
        }
    }
}
