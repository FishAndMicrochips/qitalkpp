/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtGraphicalEffects 1.0
import fishandmicrochips.qitalk.database 1.0
import fishandmicrochips.qitalk.api 1.0

Page {
    id: root
    property string m_contact_id
    property string name:  contactsDatabase.name(m_contact_id)
    property string num: contactsDatabase.num(m_contact_id)
    readonly property bool is_console: name === "CONSOLE"

    header: ToolBar {
        Material.background: Material.color(Material.Shade600)
        ToolButton {
            text: "<"
            anchors.left: parent.left
            anchors.leftMargin: 10
            font.pixelSize: 20
            font.bold: true
            anchors.verticalCenter: parent.verticalCenter
            onClicked: root.StackView.view.pop()
        }
        Label {
            id: pageTitle
            text: name
            font.pixelSize: 20
            anchors.centerIn: parent
        }
        RoundButton {
            id: callButton
            font.pixelSize: 20
            font.bold: true
            text: "☎"
            flat: true
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            visible: !is_console
            onClicked: api.dial(num);
        }
    }

    ColumnLayout {
        anchors.fill: parent
        ListView {
            id: listView
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: pane.leftPadding + messageField.leftPadding
            displayMarginBeginning: 40
            displayMarginEnd: 40
            verticalLayoutDirection: ListView.BottomToTop
            spacing: 12
            model: messageDatabase
            delegate: Column {
                readonly property int sent_by_me: model.by_me
                // 0: Left, 1: Right, 2: Center
                anchors.left:  sent_by_me == 0? parent.left : undefined
                anchors.right: sent_by_me == 1? parent.right : undefined
                anchors.horizontalCenter: sent_by_me == 2? parent.horizontalCenter : undefined

                Row {
                    id: messageRow
                    spacing: 6
                    anchors.left:  sent_by_me == 0? parent.left : undefined
                    anchors.right: sent_by_me == 1? parent.right : undefined
                    anchors.horizontalCenter: sent_by_me == 2? parent.horizontalCenter : undefined
                    // Message Body
                    Rectangle {
                        width: Math.min(messageText.implicitWidth + 24, listView.width - (sent_by_me > 0? 0 : messageRow.spacing))
                        height: messageText.implicitHeight + 24
                        color: sent_by_me == 1? "lightgray":
                               sent_by_me == 2? "darkgray":
                                                "steelblue" // TODO: don't make this hardcoded?
                        radius: 5

                        Label {
                            id: messageText
                            text: model.message
                            font.pixelSize: 15
                            color: sent_by_me == 0? "white": "black" // Don't make this hardcoded?
                            anchors.fill: parent
                            anchors.margins: 12
                            wrapMode: Label.Wrap
                        }
                    }
                }
                // Timestamp
                Label {
                    id: timestampText
                    text: Qt.formatDateTime(model.timestamp, "d MMM hh:mm")
                    horizontalAlignment: sent_by_me == 0? Text.AlignLeft:
                                         sent_by_me == 1? Text.AlignRight:
                                                          Text.AlignHCenter
                    color: "darkgray"
                    // 0: Left, 1: Right, 2: Center
                    anchors.left:  sent_by_me == 0? parent.left  : undefined
                    anchors.right: sent_by_me == 1? parent.right : undefined
                    anchors.horizontalCenter: sent_by_me == 2? parent.horizontalCenter : undefined
                    width: parent.width
                }
            }
            ScrollBar.vertical: ScrollBar { visible: false }
        }

        /* This is where you input your message */
        Pane {
            id: pane
            Layout.fillWidth: true
            RowLayout {
                width: parent.width
                /* The Input Field */
                TextArea {
                    id: messageField
                    Layout.fillWidth: true
                    placeholderText: "Compose Message"
                    wrapMode: TextArea.Wrap
                }

                /* The Send Button */
                Button {
                    id: sendButton
                    text: "✔️"
                    Material.background: Material.color(Material.Green)
                    font.pixelSize: 20
                    enabled: messageField.length > 0
                    onClicked: {
                        if (is_console) {
                            api.write(messageField.text);
                        } else {
                            api.sendSMS(num, messageField.text);
                        }
                        messageField.text = "";
                    }
                }
            }
        }
    }
}
