/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3

Page {
    id: root
    header: ToolBar {
        id: toolbar
        Material.background: Material.color(Material.Shade300)
        Label {
            id: pageTitle
            text: "QiTalk"
            font.pixelSize: 20
            anchors.centerIn: parent
        }
    }
    GridLayout {
        id: grid
        anchors.fill: parent
        width: parent.width
        columns: 3
        readonly property int buttonSize: 90
        AppButton {
            text: "☎"
            urlToPush: "qrc:/Contacts.qml";
        }
        AppButton {
            text: "WWW"
            urlToPush: "qrc:/WebBrowser.qml";
        }
        AppButton {
            text: ">_"
            urlToPush: "qrc:/Terminal.qml";
        }
    }
}
