/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.3
import QtWebKit 3.0

Page {
    id: root
    readonly property string homepage: "https://duckduckgo.com"
    property string url: homepage;
    header: ToolBar {
        Material.background: Material.color(Material.Green)
        ToolButton {
            text: "<"
            anchors.left: parent.left
            anchors.leftMargin: 10
            font.pixelSize: 20
            font.bold: true
            anchors.verticalCenter: parent.verticalCenter
            onClicked: root.StackView.view.pop()
        }
        Label {
            readonly property int maxlen: 35
            id: pageTitle
            text: webView.title.length > maxlen? webView.title.slice(0, maxlen-4) + "..." : root.url
            font.pixelSize: 20
            anchors.centerIn: parent
        }
    }
    Column {
        id: lay
        anchors.fill: parent
        Pane {
            id: topBar
            width: parent.width
            Row {
                width: parent.width
                Row {
                    id: key_buttons
                    RoundButton {
                        id: backButton
                        text: "<"
                        Material.background: Material.color(Material.Green)
                        onClicked: webView.goBack()
                    }
                    RoundButton {
                        id: fwdButton
                        text: ">"
                        Material.background: Material.color(Material.Green)
                        onClicked: webView.goForward()
                    }
                    RoundButton {
                        id: reloadButton
                        text: "R"
                        Material.background: Material.color(Material.Green)
                        onClicked: webView.reload()
                    }
                }
                TextArea {
                    id: searchField
                    width: parent.width - goButton.width - key_buttons.width
                    placeholderText: root.homepage
                }
                RoundButton {
                    id: goButton
                    text: qsTr("GO")
                    flat: true
                    Material.background: Material.color(Material.Green)
                    onClicked: {
                        root.url = searchField.text == ""? root.homepage : searchField.text;
                    }
                }
            }
        }
        MouseArea {
            width: parent.width
            height: parent.height - topBar.height - bottomBar.height
            WebView {
                anchors.fill: parent
                id: webView
                contentHeight: 3000
                contentWidth: 3000
                url: root.url
            }
        }

        Pane {
            id: bottomBar
            width: parent.width
            Row {
                id: bottomBarLay
                anchors.fill: parent
                ProgressBar {
                    id: progressBar
                    width: parent.width
                    anchors.verticalCenter: parent.verticalCenter
                    visible: webView.loading
                    value: webView.loadProgress == 100? 0: webView.loadProgress
                }
            }
        }
    }
}
