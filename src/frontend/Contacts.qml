/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0

import fishandmicrochips.qitalk.database 1.0

Page {
    id: root
    property bool is_initial
    header: ToolBar {
        Material.background: Material.color(Material.Shade100)
        Row {
            anchors.left: parent.left
            RoundButton {
                id: wwwButton
                flat: true
                text: "www"
                font.pixelSize: 20
                font.bold: true
                onClicked: root.StackView.view.push("qrc:/WebBrowser.qml", {});
            }
        }

        ToolButton {
            text: "<"
            font.pixelSize: 20
            font.bold: true
            onClicked: root.StackView.view.pop()
            visible: !is_initial // is_initial removes the toolbutton in the top left
        }
        Row {
            anchors.right: parent.right
            RoundButton {
                id: callButton
                text: "☎"
                flat: true
                font.bold: true
                font.pixelSize: 20
                onClicked: root.StackView.view.push("qrc:/NewCall.qml", {})
            }
            RoundButton {
                id: newButton
                flat: true
                text: "+"
                font.pixelSize: 20
                font.bold: true
                onClicked: contactsDatabase.newContact()
            }
        }
        Label {
            text: "Contacts"
            font.pixelSize: 20
            anchors.centerIn: parent
        }
    }

    // Main layout
    Column {
        anchors.fill: parent
        width: parent.width

        ListView {
            id: listView
            //anchors.fill: parent
            width: parent.width
            height: parent.height - newButton.height
            topMargin   : 12
            bottomMargin: 12
            leftMargin  : 12
            rightMargin : 12
            spacing: 20
            model: contactsDatabase
            delegate: contactsEntry

        }
    }

    // The ListView makes a list of these, displaying the relevant info and providing a dropdown
    Component {
        id: contactsEntry
        Column {
            id: contactsEntryColumn
            ItemDelegate {
                id: itemDelegate
                readonly property int iconSize: 48
                readonly property int iconPadding: 16
                text: model.name
                font.pixelSize: 20
                width: listView.width - listView.leftMargin - listView.rightMargin
                height: iconSize
                leftPadding: iconPadding + iconSize
                onClicked: {
                    messageDatabase.recipient = model.id
                    textNotifier.turned_on = false;
                    root.StackView.view.push ("qrc:/Text.qml", {m_contact_id: model.id});
                }
                Image {
                    id: avatar
                    source: model.icon
                    width:  itemDelegate.iconSize
                    height: itemDelegate.iconSize
                    fillMode: Image.PreserveAspectCrop
                    antialiasing: true
                    layer.enabled: true
                    smooth: true

                    layer.effect: GaussianBlur {
                        id: pretty_blur
                        anchors.fill: avatar
                        source: avatar
                        layer.enabled: true
                        radius: 1.5
                        layer.effect: OpacityMask {
                            id: make_me_a_circle
                            anchors.fill: avatar
                            source: pretty_blur
                            maskSource: Rectangle {
                                width: avatar.width
                                height: avatar.height
                                radius: 250
                                visible: false
                            }
                        }
                    }
                }
                RoundButton {
                    id: iconButton
                    width:  itemDelegate.iconSize
                    flat: true
                    height: itemDelegate.iconSize
                    onClicked: iconFinder.open()
                }

                // A set of buttons to do things to the contact
                Row {
                    id: entry_row_ly
                    anchors.right: itemDelegate.right
                    readonly property int icon_font_size: 20
                    RoundButton {
                        id: callButton
                        width:  itemDelegate.iconSize
                        height: itemDelegate.iconSize
                        font.pixelSize: entry_row_ly.icon_font_size
                        font.bold: true
                        text: "☎"
                        flat: true
                        onClicked: api.dial(contactsDatabase.num(model.id));
                    }
                    RoundButton {
                        id: editButton
                        width:  itemDelegate.iconSize
                        height: itemDelegate.iconSize
                        font.pixelSize: entry_row_ly.icon_font_size
                        font.bold: true
                        text: "✎"
                        flat: true
                        onClicked: {
                            editMenu.visible ^= true
                            areYouSure.visible &= false
                            nameEdit.text = ""
                            numEdit.text = ""
                        }
                    }
                    RoundButton {
                        id: trashButton
                        width:  itemDelegate.iconSize
                        height: itemDelegate.iconSize
                        font.pixelSize: entry_row_ly.icon_font_size
                        font.bold: true
                        text: "X"
                        flat: true
                        Material.background: Material.color(Material.Shade100)
                        onClicked: {
                            areYouSure.visible ^= true
                            editMenu.visible &= false
                        }
                    }
                }
            }
            /* Useful stuff that emerges beneath the contact */
            // For finding icons
            FileDialog {
                width: window.width
                height: window.height
                id: iconFinder
                title: "Choose Your Icon"
                folder: shortcuts.pictures
                nameFilters: ["Image Files (*.jpg *.jpeg *.png)"]
                onAccepted: {
                    const new_icon = iconFinder.fileUrl
                    if (new_icon !== avatar.source) {
                        contactsDatabase.setIcon(model.id, new_icon)
                        model.icon = new_icon
                        avatar.source = new_icon
                    }
                }
            }
            // For editing the contact
            Row {
                id: editMenu
                visible: false
                width: parent.width
                spacing: 12
                Column {
                    id: editFields
                    TextArea {
                        id: nameEdit
                        placeholderText: model.name
                        text: model.name
                        width: editMenu.width - editDone.width - editMenu.spacing
                    }
                    TextArea {
                        id: numEdit
                        placeholderText: model.num
                        text: model.num
                        width: nameEdit.width
                    }
                }
                Button {
                    id: editDone
                    text: "✔️"
                    width: 40
                    height: editFields.height
                    font.pixelSize: 20
                    Material.background: Material.color(Material.Green)
                    onClicked: {
                        contactsDatabase.editRequest(model.id, nameEdit.text, nameEdit.placeholderText, numEdit.text, numEdit.placeholderText);
                        editMenu.visible = false;
                    }
                }
            }

            // For deleting the contact
            Row {
                id: areYouSure
                visible: false
                anchors.right: parent.right
                Label {
                    text: "?"
                    font.pixelSize: 40
                    font.bold: true
                }
                RoundButton {
                    id: aysNoButton
                    width:  itemDelegate.iconSize
                    height: itemDelegate.iconSize
                    font.pixelSize: entry_row_ly.icon_font_size
                    font.bold: true
                    text: "X"
                    onClicked: areYouSure.visible ^= true
                }
                RoundButton {
                    id: aysYesButton
                    width:  itemDelegate.iconSize
                    height: itemDelegate.iconSize
                    font.pixelSize: entry_row_ly.icon_font_size
                    font.bold: true
                    text: "✔️"
                    onClicked:  contactsDatabase.deleteContact(model.id);
                }
            }
        }
    }
}
