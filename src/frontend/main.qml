/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

import QtQuick 2.9
//import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import fishandmicrochips.qitalk.api 1.0


//Window {
ApplicationWindow {
    id: window
    visible: true
    width: 470
    height: 790
    minimumHeight: 470
    minimumWidth: 470
    visibility: "Maximized"
    title: "QiTalk"
    CallNotification{
        x: 0
        y: 0
        z: 99
        id: callNotifier
        objectName: "callNotifer"
        width: parent.width
    }
    TextNotification{
        id: textNotifier
        x: 0
        y: callNotifier.visible? callNotifier.height + 5 : 0
        z: 98
        objectName: "textNotifer"
        width: parent.width
        turned_on: true
    }
    Drawer {
        id: diagnoticsDrawer
        dragMargin: 20
        height: window.height
        width: 0.8*window.width
        Diagnostics {}
    }

    Drawer {
        id: settingsDrawer
        dragMargin: 20
        height: window.height
        width: 0.8*window.width
        edge: Qt.RightEdge
        Settings{}
    }

    StackView {
        x: 0
        y: 0
        id: stack
        anchors.fill: parent
        width: parent.width
        //height: inputPanel.y
        height: parent.height
        initialItem:
            //AppMenu {}
            Contacts { is_initial: true } // is_initial removes the toolbutton in the top left

        Connections {
            target: api
            onCallStarted: {
                const num = api.call_num;
                const contact_id = contactsDatabase.getId(num);
                var name = contactsDatabase.name(num, "num");
                if (name === "<Insert name>") {
                    name = num;
                }
                textNotifier.turned_on = false;
                stack.push("qrc:/Call.qml", { width: stack.width, height: stack.height });
            }
            onCallEnded: {
                textNotifier.turned_on = true;
            }
        }
    }
    //InputPanel {
    //    id: inputPanel
    //    z: 99
    //    x: 0
    //    y: window.height
    //    width: window.width
    //    states: State {
    //        name: "visible"
    //        when: inputPanel.active
    //        PropertyChanges {
    //            target: inputPanel
    //            y: window.height - inputPanel.height
    //        }
    //    }
    //    transitions: Transition {
    //        from: ""
    //        to: "visible"
    //        reversible: true
    //        ParallelAnimation {
    //            NumberAnimation {
    //                properties: "y"
    //                duration: 500
    //                easing.type: Easing.InOutQuad
    //            }
    //        }
    //    }
    //}
}
