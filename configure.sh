#!/bin/bash
# QiTalk: A QT-Based Chat Application for the Quectel-UC15
# Copyright (C) 2020 Amber Gingell
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

# First, check for dependencies
sudo apt-get install \
    xserver-xorg-input-evdev\
    ppp\
    libqt5serialport5-dev\
    qml-module-qtqml-models2\
    qml-module-qtquick-controls\
    qml-module-qtquick-controls2\
    qml-module-qtquick-dialogs\
    qml-module-qtquick-extras\
    qml-module-qtquick-layouts\
    qml-module-qtquick-privatewidgets\
    qml-module-qtquick-scene2d\
    qml-module-qtquick-shapes\
    qml-module-qtquick-templates2\
    qml-module-qtquick-virtualkeyboard\
    qml-module-qtquick-window2\
    qml-module-qt-labs-settings\
    qt5-default\
    qt5-image-formats-plugins\
    qt5-qmake-bin\
    qt5-qmake\
    qt5-qmltooling-plugins\
    qt5-style-plugins\
    qt5ct\
    qtbase5-dev-tools\
    qtbase5-dev\
    qtbase5-doc\
    qtconnectivity5-doc\
    qtchooser\
    qtdeclarative5-dev-tools\
    qtdeclarative5-dev\
    qtquickcontrols2-5-dev\
    qttools5-dev\
    qtvirtualkeyboard-plugin\
    qml-module-qtwebkit\
    qml-module-qt-labs-folderlistmodel

DIR=$(dirname $0)

# Then, we copy any relevant data to ~/.local/share
mkdir ${HOME}/.local/share/QiTalk
cp -r $DIR/icons ${HOME}/.local/share/QiTalk/icons

# Finally, we generate the makefile.
qmake ${DIR}/QiTalk.pro -o ${DIR}/Makefile

echo "Now go into raspi-config and go to interfacing->serial. Enable the serial hardware if you are going to talk to the pitalk over GPIO."
